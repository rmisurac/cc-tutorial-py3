from django.test import TestCase
from django.urls import reverse
from polls import views

# Create your tests here.
class PollsTests(TestCase):
    """
    Simple tests for the polls app
    """

    def test_choices(self):
        """Test the choices view"""

        response = self.client.get(reverse('polls:choices'))
        self.assertEqual(response.status_code, 200)

    def test_questions(self):
        """Test the questions view"""

        response = self.client.get(reverse('polls:questions'))
        self.assertEqual(response.status_code, 200)

    def test_most_popular_choice(self):

        response = self.client.get(reverse('polls:most-popular-choice'))
        self.assertEqual(response.status_code, 200)

    def create_question(question_text, days):
        """
        Create a question with the given `question_text` and published the
        given number of `days` offset to now (negative for questions published
        in the past, positive for questions that have yet to be published).
        """
        time = timezone.now() + datetime.timedelta(days=days)
        return Question.objects.create(question_text=question_text, pub_date=time)

