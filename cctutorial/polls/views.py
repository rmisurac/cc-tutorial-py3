from datetime import date, datetime, time
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.template import loader, RequestContext
from django.shortcuts import render_to_response, render, get_object_or_404
from polls.models import Choice, Question
#from django.utils.timezone import make_aware
from django.utils.timezone import now
from django.utils import timezone
from django.urls import reverse

#directed here from an empty path
def index(request):
    return render(request, 'polls/index.html')

#Queried once for all Question objects
def my_index(request):
    questions = Question.objects.all().order_by('pub_date')
    return render(request, 'polls/my_index.html', {'questions': questions})

#this is the "home" page for polls
#Had to rollback Django version to use render_to_response
def home(request):
    latest_question_list = Question.objects.only('question_text').order_by('-pub_date')[:5]
    output = ', '.join([p.question_text for p in latest_question_list])
    template = loader.get_template('polls/home.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return render_to_response('polls/home.html', context, request)

def most_popular_choice(request):
    choices = Choice.objects.all().order_by('votes').reverse()
    most_popular_choices = []
    max = choices[0].votes

    for choice in choices:
        if choice.votes >= max:
            most_popular_choices.append(choice)

    rendered = render_to_string('polls/most_popular_choice.html', {'most_popular_choices': most_popular_choices})
    return HttpResponse(rendered)


def choices(request, pk=None):

    if pk:
        #even though choices will only contain one choice in this case,
        #I made it a list to align with the list in the else block
        choice = get_object_or_404(Choice, pk=pk)
        choices = []
        choices.append(choice)
        questions = []
        questions = Question.objects.get(pk=choices[0].question_id)
        rendered = render_to_string('polls/choices.html', {'choices': choices, 'questions': questions})
        return HttpResponse(rendered)
    else:
        #Is there a better way to order these choices? 
        #using order_by.reverse() seems funky, but it works
        choices = Choice.objects.all().order_by('votes').reverse()
        questions = []

        rendered = render_to_string('polls/choices.html', {'choices': choices, 'questions': questions})
        return HttpResponse(rendered)


def questions(request, pub_date=None):

    #I used the full range of possible times to filter by a date at any time
    #To filter by date, the date should be entered in the url as YYYY-MM-DD
    # i.e.(..../questions/2021-07-10)
    if pub_date:
        pub_date_early = datetime.strptime(pub_date, "%Y-%m-%d").replace(hour=0, minute=0)
        pub_date_late = datetime.strptime(pub_date, "%Y-%m-%d").replace(hour=23, minute=59)
        questions = Question.objects.filter(pub_date__gte=pub_date_early).filter(pub_date__lte=pub_date_late)
    
    #This shows all questions published before the current datetime, effectively
    #showing all questions
    else:
        questions = Question.objects.filter(pub_date__lte=datetime.today())

    rendered = render_to_string('polls/questions.html', {'questions': questions})
    return HttpResponse(rendered)

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/vote.html', {
            'question': question,
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:voter_limit'))

def voter_limit(request):
    return render(request, 'polls/voter_limit.html')
