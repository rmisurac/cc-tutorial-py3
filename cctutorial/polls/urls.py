from django.urls import path
from datetime import datetime

from polls import views

app_name = 'polls'

urlpatterns = [
    path('', views.home, name='polls'),
    path('index/', views.my_index, name='my_index'),
    path('most-popular/', views.most_popular_choice, name='most-popular-choice'),
    path('choices/<int:pk>/', views.choices, name='choices'),
    path('choices/', views.choices, name='choices'),
    path('questions/', views.questions, name='questions'),
    path('questions/<pub_date>', views.questions, name='questions'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('voter_limit/', views.voter_limit, name='voter_limit'),
]
